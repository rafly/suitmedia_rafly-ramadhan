package com.example.suitmedia_rafly;

import org.json.JSONException;
import org.json.JSONObject;

public class UserModel {
    private int id;
    private String email;
    private String first_name;
    private String last_name;
    private String avatar;
    private String full_name;

    public String getFull_name() {
        return first_name +" "+last_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public static UserModel fromJson(JSONObject jsonObject) throws JSONException {
        UserModel user = new UserModel();
        user.setId(jsonObject.getInt("id"));
        user.setEmail(jsonObject.getString("email"));
        user.setFirst_name(jsonObject.getString("first_name"));
        user.setLast_name(jsonObject.getString("last_name"));
        user.setAvatar(jsonObject.getString("avatar"));
        return user;
    }
}

