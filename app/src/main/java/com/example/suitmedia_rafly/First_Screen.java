package com.example.suitmedia_rafly;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class First_Screen extends AppCompatActivity {
    EditText name, palindrome;
    Button check, next;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_screen);

        name = findViewById(R.id.etName);
        palindrome = findViewById(R.id.etPalindrome);
        check = findViewById(R.id.btCheck);
        next = findViewById(R.id.btNext);

        databaseHelper = new DatabaseHelper(this);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String enteredName = name.getText().toString();
                saveToDatabase(enteredName);
                Intent toSecondScreen = new Intent(First_Screen.this, Second_Screen.class);
                toSecondScreen.putExtra("enteredName", enteredName);
                startActivity(toSecondScreen);
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputString = palindrome.getText().toString();
                if (isPalindrome(inputString)) {
                    showResultDialog("Is Palindrome");
                } else {
                    showResultDialog("Not Palindrome");
                }
            }
        });
    }

    // Save enteredName to the database
    private void saveToDatabase(String enteredName) {
        long result = databaseHelper.saveName(enteredName);

        if (result <= 0) {
            Toast.makeText(this, "Failed to save name", Toast.LENGTH_SHORT).show();
        }
    }

    // Check palindrome
    private boolean isPalindrome(String str) {
        str = str.toLowerCase().replaceAll("[^a-zA-Z0-9]", "");

        int left = 0;
        int right = str.length() - 1;

        while (left < right) {
            if (str.charAt(left) != str.charAt(right)) {
                return false;
            }
            left++;
            right--;
        }

        return true;
    }

    // Pop up result dialog
    private void showResultDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
