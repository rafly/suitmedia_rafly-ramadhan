package com.example.suitmedia_rafly;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Third_Screen extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UserAdapter userAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int currentPage = 1;
    int totalPage;
    ImageButton btBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third_screen);

        recyclerView = findViewById(R.id.recycler);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        btBack = findViewById(R.id.btBack);

        btBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Refresh
        swipeRefreshLayout.setOnRefreshListener(() -> {
            refreshData();
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!recyclerView.canScrollVertically(1)) {
                    // Load more data when scrolling to the bottom and currentPage is less than totalPage
                    loadMoreData();
                }
            }
        });
        loadData();
    }

    private void loadData() {
        new FetchDataTask().execute("https://reqres.in/api/users?page=" + currentPage + "&per_page=10");
    }

    private void refreshData() {
        currentPage = 1;
        new FetchDataTask().execute("https://reqres.in/api/users?page=" + currentPage + "&per_page=10");
    }

    private void loadMoreData() {
        if (currentPage < totalPage) {
            currentPage++;
            new FetchDataTask().execute("https://reqres.in/api/users?page=" + currentPage + "&per_page=10");
        }
    }

    private class FetchDataTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    InputStream in = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    return result.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    totalPage = jsonObject.getInt("total_pages");
                    JSONArray dataArray = jsonObject.getJSONArray("data");
                    List<UserModel> userList = new ArrayList<>();

                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject userObject = dataArray.getJSONObject(i);

                        int userId = userObject.getInt("id");
                        String userEmail = userObject.getString("email");
                        String firstName = userObject.getString("first_name");
                        String lastName = userObject.getString("last_name");
                        String avatarUrl = userObject.getString("avatar");

                        UserModel user = new UserModel();
                        user.setId(userId);
                        user.setEmail(userEmail);
                        user.setFirst_name(firstName);
                        user.setLast_name(lastName);
                        user.setAvatar(avatarUrl);
                        userList.add(user);
                    }

                    userAdapter = new UserAdapter(Third_Screen.this, userList);
                    recyclerView.setAdapter(userAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        }
    }
}
