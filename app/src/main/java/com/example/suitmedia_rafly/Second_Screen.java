package com.example.suitmedia_rafly;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Second_Screen extends AppCompatActivity {

    // Intent extra constants
    public static final String EXTRA_FULL_NAME = "fullName";
    public static final String EXTRA_ENTERED_NAME = "enteredName";

    TextView name, userName;
    ImageButton backButton;
    Button chooseUserButton;
    DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_screen);

        name = findViewById(R.id.name);
        userName = findViewById(R.id.userName);
        backButton = findViewById(R.id.btBack);
        chooseUserButton = findViewById(R.id.btChooseUser);

        // Initialize DatabaseHelper
        databaseHelper = new DatabaseHelper(this);

        Intent intent = getIntent();
        String fullName = intent.getStringExtra(EXTRA_FULL_NAME);
        if (fullName != null) {
            userName.setText(fullName);
        }

        // Retrieve name from the database
        String savedName = databaseHelper.getSavedName();
        if (savedName != null) {
            name.setText(savedName);
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toFirstScreen = new Intent(Second_Screen.this, First_Screen.class);
                startActivity(toFirstScreen);
                finish();
            }
        });

        chooseUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toChoose = new Intent(Second_Screen.this, Third_Screen.class);
                startActivity(toChoose);
            }
        });
    }
}
